from resources.playlist_util import user_playlist


def print_user_playlist():
    playlist = user_playlist()
    msg = "Playlist tracks"
    print("\n", msg.center(55, "="))
    for i in playlist['items']:
        print(f"{i['track']['name']} - by"
              f" {i['track']['artists'][0]['name']}")


print_user_playlist()
