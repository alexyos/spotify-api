# README #

### Summary ###
Scripts to query Spotify and get album list by an artist, registered user
 playlist and script that compares top 10 tracks by any given artist with
  user playlist.


### How do I get set up? ###

* Need to log in https://developer.spotify.com/dashboard/applications
* Create your app name
* Get client ID and Secret and put it in data.json file
* At the first run you will be required to copy and paste url from the
 redirect url to obtain an authorization token.