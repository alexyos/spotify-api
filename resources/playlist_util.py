import requests
from resources.auth import data_utils, authorize


def user_playlist():
    with open("resources/cred.txt", "r") as f:
        token = f.read()
    headers = {
        "Authorization": f"Bearer {token}",
        "scope": "playlist-read-private"
    }
    endpoint = data_utils()['user_playlist_url']

    response = requests.get(endpoint, headers=headers)
    if response.status_code == 401:
        authorize()
        playlist = user_playlist()
        return playlist

    assert response.status_code == 200, f"Failed to request. Response: " \
                                        f"{response.status_code}"
    page_json = response.json()

    # getting playlist id form 1st playlist
    playlist_id = page_json["items"][0]['id']
    endpoint = data_utils()['playlist_url']
    uri = endpoint + playlist_id + '/' + 'tracks'
    headers = {
        "Authorization": f"Bearer {token}",
        "scope": "user-library-read"}
    response1 = requests.get(uri, headers=headers)

    assert response.status_code == 200, f"Failed to request. Response: " \
                                        f"{response.status_code}"
    playlist = response1.json()

    return playlist

