import json
from rauth import OAuth2Service


def data_utils():
    with open('resources/data.json', 'r') as file:
        data = json.loads(file.read())
    return data


def authorize():
    spotify = OAuth2Service(
        name=data_utils()['app_name'],
        client_id=data_utils()['client_id'],
        client_secret=data_utils()['secret'],
        access_token_url=data_utils()['access_url'],
        authorize_url=data_utils()['authorize_url'],
        base_url=data_utils()["search_url"])
    params = {'redirect_uri': data_utils()['redirect_url'],
              'response_type': 'token',
              'scope': 'playlist-read-private'}
    url = spotify.get_authorize_url(**params)
    print(url)
    auth_response = input('PLease click on the link and copy/paste the full '
                          'callback URL\n')
    with open("resources/cred.txt", "w") as f:
        f.write(auth_response[(auth_response.find('=') +
                               1):auth_response.find('&')])
