
import requests
from urllib.parse import urlencode
from resources.auth import data_utils, authorize


def album_list(artist):
    with open("resources/cred.txt", "r") as f:
        token = f.read()
    headers = {
        "Authorization": f"Bearer {token}"
    }
    endpoint = data_utils()['search_url']
    params = urlencode({"q": artist, "type": "album"})
    lookup_url = f"{endpoint}?{params}"
    response = requests.get(lookup_url, headers=headers)
    if response.status_code == 401:
        authorize()
        album_list(artist)
        return album_list
    assert response.status_code == 200, f"Failed to request. Response: " \
                                        f"{response.status_code}"
    artist_id = response.json()['albums']['items'][0]['artists'][0]['name']

    msg = f" {artist_id} albums "
    print("\n", msg.center(55, "="))
    for i in response.json()['albums']['items']:
        print(f"{i['name']} Release date:{i['release_date']}")


group_or_artist = input("\n Please enter the artist name:\n")
album_list(group_or_artist)
