import requests
from resources.auth import authorize, data_utils
from resources.playlist_util import user_playlist
from urllib.parse import urlencode


def get_top10_tracks(artist):
    with open("resources/cred.txt", "r") as f:
        token = f.read()
    headers = {
        "Authorization": f"Bearer {token}",
        "scope": "user-library-read"}

    endpoint = data_utils()['search_url']
    data = urlencode({"q": artist, "type": "album"})
    lookup_url = f"{endpoint}?{data}"
    response = requests.get(lookup_url, headers=headers)
    if response.status_code == 401:
        authorize()
        get_top10_tracks(artist)
        return
    assert response.status_code == 200, f"Failed to request. Response: " \
                                        f"{response.status_code}"
    page_json = response.json()
    artist_id = page_json['albums']['items'][0]['artists'][0]['id']
    endpoint = data_utils()['artist_url'] + artist_id + "/top-tracks"
    params = {"market": "us"}
    response = requests.get(endpoint, headers=headers, params=params)
    assert response.status_code == 200, f"Failed to request. Response: " \
                                        f"{response.status_code}"
    top_10 = []
    msg = f"Top 10 popular tracks by {artist}"
    print("\n", msg.center(55, "="))
    for i in response.json()['tracks']:
        print(f"{i['name']} - popularity:"
              f"{i['popularity']}")
        top_10.append(i['name'])
    play_list = user_playlist()
    playlist = []
    for i in play_list['items']:
        playlist.append(i['track']['name'])
    if set(playlist).intersection(set(top_10)):
        print(f"\nFollowing tracks from the top 10 by the {artist} are in "
              f"user playlist:")
        print(*set(playlist).intersection(set(top_10)), sep="\n")
    else:
        print(f"\nUser playlist doesnt contain any popular tracks from"
              f" {artist}")


# Enter the name of the artist to get top 10 track and see if any of them in
# your playlist.
group_or_artist = input("\n Please enter the artist name:\n")
get_top10_tracks(group_or_artist)
